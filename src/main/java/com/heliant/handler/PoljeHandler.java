package com.heliant.handler;

import com.heliant.model.Polje;
import com.heliant.model.dto.CreatePoljeRequest;
import com.heliant.model.dto.UpdatePoljeRequest;
import com.heliant.service.PoljeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PoljeHandler {

    private final PoljeService poljeService;

    public PoljeHandler(PoljeService poljeService) {
        this.poljeService = poljeService;
    }

    public ResponseEntity<?> create(CreatePoljeRequest createPoljeRequest) {

        return poljeService.create(createPoljeRequest);
    }

    public ResponseEntity<?> find(Integer id) {

        Optional<Polje> optionalPolje = poljeService.find(id);
        if (optionalPolje.isPresent()) {
            Polje polje = optionalPolje.get();
            return new ResponseEntity<>(polje, HttpStatus.OK);
        }

        return new ResponseEntity<>("Polje nije pronadjeno", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> update(Integer id, UpdatePoljeRequest updatePoljeRequest) {

        return poljeService.update(id, updatePoljeRequest);
    }

    public ResponseEntity<String> delete(Integer id) {
        return poljeService.delete(id);
    }
}
