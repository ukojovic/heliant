package com.heliant.handler;

import com.heliant.model.Statistika;
import com.heliant.model.dto.CreateStatistikaRequest;
import com.heliant.model.dto.EntityResponse;
import com.heliant.model.dto.StatistikaResource;
import com.heliant.model.dto.UpdateStatistikaRequest;
import com.heliant.service.StatistikaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class StatistikaHandler {

    private final StatistikaService statistikaService;

    public StatistikaHandler(StatistikaService statistikaService) {
        this.statistikaService = statistikaService;
    }

    public EntityResponse<StatistikaResource> create(CreateStatistikaRequest createStatistikaRequest) {

        Statistika statistika = statistikaService.create(createStatistikaRequest);

        StatistikaResource statistikaResource = StatistikaResource.builder()
                .id(statistika.getId())
                .datum(statistika.getDatum())
                .brojPopunjenihFormulara(statistika.getBrojPopunjenihFormulara())
                .build();

        return EntityResponse.<StatistikaResource>builder()
                .result(List.of(statistikaResource))
                .totalCount(1L)
                .build();
    }

    public ResponseEntity<?> find(Integer id) {

        Optional<Statistika> optionalStatistika = statistikaService.find(id);

        if (optionalStatistika.isPresent()) {
            Statistika statistika = optionalStatistika.get();
            return new ResponseEntity<>(statistika, HttpStatus.OK);
        }

        return new ResponseEntity<>("Statistika nije pronadjena", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> update(Integer id, UpdateStatistikaRequest updateStatistikaRequest) {

        Optional<Statistika> optionalStatistika = statistikaService.find(id);

        if (optionalStatistika.isPresent()) {
            Statistika updatedStatistika = statistikaService.update(optionalStatistika.get(), updateStatistikaRequest);
            StatistikaResource statistikaResource = StatistikaResource.builder()
                    .id(updatedStatistika.getId())
                    .brojPopunjenihFormulara(updatedStatistika.getBrojPopunjenihFormulara())
                    .datum(updatedStatistika.getDatum())
                    .build();
            return new ResponseEntity<>(statistikaResource, HttpStatus.OK);
        }
        return new ResponseEntity<>("Statistika nije pronadjena", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<String> delete(Integer id) {
        return statistikaService.delete(id);
    }

}
