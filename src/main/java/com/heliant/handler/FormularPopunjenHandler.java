package com.heliant.handler;

import com.heliant.model.FormularPopunjen;
import com.heliant.model.dto.CreateFormularPopunjenRequest;
import com.heliant.service.FormularPopunjenService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class FormularPopunjenHandler {

    private final FormularPopunjenService formularPopunjenService;

    public FormularPopunjenHandler(FormularPopunjenService formularPopunjenService) {
        this.formularPopunjenService = formularPopunjenService;
    }

    public ResponseEntity<?> create(CreateFormularPopunjenRequest createFormularPopunjenRequest) {

        return formularPopunjenService.create(createFormularPopunjenRequest);
    }

    public ResponseEntity<?> find(Integer id) {

        Optional<FormularPopunjen> optionalFormularPopunjen = formularPopunjenService.find(id);

        if (optionalFormularPopunjen.isPresent()) {
            FormularPopunjen formularPopunjen = optionalFormularPopunjen.get();
            return new ResponseEntity<>(formularPopunjen, HttpStatus.OK);

        }

        return new ResponseEntity<>("Formular popunjen nije pronadjen", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> update(Integer id, Integer formularId) {

        Optional<FormularPopunjen> optionalFormularPopunjen = formularPopunjenService.find(id);
        if (optionalFormularPopunjen.isPresent()) {
            FormularPopunjen formularPopunjen = optionalFormularPopunjen.get();

            formularPopunjenService.update(formularPopunjen, formularId);
        }
        return new ResponseEntity<>("Formular popunjen nije pronadjen", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<String> delete(Integer id) {
        return formularPopunjenService.delete(id);
    }
}
