package com.heliant.handler;

import com.heliant.model.Formular;
import com.heliant.model.dto.CreateFormularRequest;
import com.heliant.model.dto.EntityResponse;
import com.heliant.model.dto.FormularResource;
import com.heliant.model.dto.PaginationResource;
import com.heliant.service.FormularService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class FormularHandler {

    private final FormularService formularService;

    public FormularHandler(FormularService formularService) {
        this.formularService = formularService;
    }

    public EntityResponse<FormularResource> create(CreateFormularRequest createFormularRequest) {

        Formular formular = formularService.create(createFormularRequest);
        FormularResource formularResource = FormularResource.builder()
                .id(formular.getId())
                .naziv(formular.getNaziv())
                .vremeKreiranja(formular.getVremeKreiranja())
                .vremePoslednjeIzmene(formular.getVremePoslednjeIzmene())
                .idKorisnikKreirao(formular.getIdKorisnikKreirao())
                .idKorisnikPoslednjiAzurirao(formular.getIdKorisnikPoslednjiAzurirao())
                .build();

        return EntityResponse.<FormularResource>builder()
                .result(List.of(formularResource))
                .totalCount(1L)
                .build();

    }

    public ResponseEntity<?> update(Integer id, String naziv) {

        Optional<Formular> optionalFormular = formularService.find(id);

        if (optionalFormular.isPresent()) {

            Formular updatedFormular = formularService.update(optionalFormular.get(), naziv);
            FormularResource formularResource = FormularResource.builder()
                    .id(updatedFormular.getId())
                    .naziv(updatedFormular.getNaziv())
                    .vremeKreiranja(updatedFormular.getVremeKreiranja())
                    .vremePoslednjeIzmene(updatedFormular.getVremePoslednjeIzmene())
                    .idKorisnikKreirao(updatedFormular.getIdKorisnikKreirao())
                    .idKorisnikPoslednjiAzurirao(updatedFormular.getIdKorisnikPoslednjiAzurirao())
                    .build();
            return new ResponseEntity<>(formularResource, HttpStatus.OK);

        }

        return new ResponseEntity<>("Formular nije pronadjen", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> find(Integer id) {

        Optional<Formular> optionalFormular = formularService.find(id);

        if (optionalFormular.isPresent()) {
            Formular formular = optionalFormular.get();
            return new ResponseEntity<>(formular, HttpStatus.OK);
        }

        return new ResponseEntity<>("Formular nije pronadjen", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<String> delete(Integer id) {
        return formularService.delete(id);
    }

    public EntityResponse<Formular> getAll(PaginationResource paginationResource, String sortBy) {
        return formularService.getAll(paginationResource, sortBy);
    }
}
