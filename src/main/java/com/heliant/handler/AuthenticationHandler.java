package com.heliant.handler;

import com.heliant.model.dto.AuthenticationRequest;
import com.heliant.model.dto.AuthenticationResponse;
import com.heliant.model.dto.RegisterRequest;
import com.heliant.service.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationHandler {

    private final AuthenticationService authenticationService;

    public AuthenticationHandler(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    public ResponseEntity<?> register(RegisterRequest request) {
        return authenticationService.register(request);
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        return authenticationService.authenticate(request);
    }
}
