package com.heliant.handler;

import com.heliant.model.PoljePopunjeno;
import com.heliant.model.dto.CreatePoljePopunjenoRequest;
import com.heliant.model.dto.UpdatePoljePopunjenoRequest;
import com.heliant.service.PoljePopunjenoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PoljePopunjenoHandler {

    private final PoljePopunjenoService poljePopunjenoService;

    public PoljePopunjenoHandler(PoljePopunjenoService poljePopunjenoService) {
        this.poljePopunjenoService = poljePopunjenoService;
    }

    public ResponseEntity<?> create(CreatePoljePopunjenoRequest createPoljePopunjenoRequest) {

        return poljePopunjenoService.create(createPoljePopunjenoRequest);
    }

    public ResponseEntity<?> find(Integer id) {

        Optional<PoljePopunjeno> optionalPoljePopunjeno = poljePopunjenoService.find(id);
        if (optionalPoljePopunjeno.isPresent()) {
            PoljePopunjeno poljePopunjeno = optionalPoljePopunjeno.get();
            return new ResponseEntity<>(poljePopunjeno, HttpStatus.OK);
        }
        return new ResponseEntity<>("Polje popunjeno nije pronadjeno", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<String> delete(Integer id) {
        return poljePopunjenoService.delete(id);
    }

    public ResponseEntity<?> update(Integer id, UpdatePoljePopunjenoRequest updatePoljePopunjenoRequest) {

        Optional<PoljePopunjeno> poljePopunjeno = poljePopunjenoService.find(id);
        if (poljePopunjeno.isEmpty()) {
            return new ResponseEntity<>("Polje popunjeno nije pronadjeno", HttpStatus.NOT_FOUND);
        }
        return poljePopunjenoService.update(poljePopunjeno.get(), updatePoljePopunjenoRequest);
    }
}
