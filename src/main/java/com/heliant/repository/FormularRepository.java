package com.heliant.repository;

import com.heliant.model.Formular;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormularRepository extends JpaRepository<Formular, Integer> {
}
