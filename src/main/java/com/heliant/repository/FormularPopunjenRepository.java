package com.heliant.repository;

import com.heliant.model.FormularPopunjen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface FormularPopunjenRepository extends JpaRepository<FormularPopunjen, Integer> {

    @Query(value = "SELECT COUNT(fp) FROM FormularPopunjen fp WHERE fp.vremeKreiranja BETWEEN :beginningOfPreviousDay AND :endOfPreviousDay")
    Long previousDayStatistics(@Param("beginningOfPreviousDay") LocalDateTime beginningOfPreviousDay,
                                      @Param("endOfPreviousDay") LocalDateTime endOfPreviousDay);
}
