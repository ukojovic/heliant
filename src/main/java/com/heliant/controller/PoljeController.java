package com.heliant.controller;

import com.heliant.handler.PoljeHandler;
import com.heliant.model.dto.CreatePoljeRequest;
import com.heliant.model.dto.UpdatePoljeRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/polje")
public class PoljeController {

    private final PoljeHandler poljeHandler;

    public PoljeController(PoljeHandler poljeHandler) {
        this.poljeHandler = poljeHandler;
    }

    @PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create polje")
    public ResponseEntity<?> create(@Valid @RequestBody CreatePoljeRequest createPoljeRequest) {
        return poljeHandler.create(createPoljeRequest);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Find polje")
    public ResponseEntity<?> find(@Valid
                                  @PathVariable("id")
                                  @Min(1)
                                  @Parameter(description = "Polje id") Integer id) {

        return poljeHandler.find(id);
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Update polje")
    public ResponseEntity<?> update(@Valid
                                    @PathVariable("id")
                                    @Min(1)
                                    @Parameter(description = "Polje id") Integer id,
                                    @Valid @RequestBody UpdatePoljeRequest updatePoljeRequest) {

        return poljeHandler.update(id, updatePoljeRequest);
    }

    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Delete polje")
    public ResponseEntity<String> delete(@Valid
                                         @PathVariable("id")
                                         @Min(1)
                                         @Parameter(description = "Polje id") Integer id) {
        return poljeHandler.delete(id);
    }

}
