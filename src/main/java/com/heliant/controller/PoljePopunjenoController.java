package com.heliant.controller;

import com.heliant.handler.PoljePopunjenoHandler;
import com.heliant.model.dto.CreatePoljePopunjenoRequest;
import com.heliant.model.dto.UpdatePoljePopunjenoRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/poljePopunjeno")
public class PoljePopunjenoController {

    private final PoljePopunjenoHandler poljePopunjenoHandler;

    public PoljePopunjenoController(PoljePopunjenoHandler poljePopunjenoHandler) {
        this.poljePopunjenoHandler = poljePopunjenoHandler;
    }

    @PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create polje popunjeno")
    public ResponseEntity<?> create(@Valid @RequestBody CreatePoljePopunjenoRequest createPoljePopunjenoRequest) {
        return poljePopunjenoHandler.create(createPoljePopunjenoRequest);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Find polje popunjeno")
    public ResponseEntity<?> find(@Valid
                                  @PathVariable("id")
                                  @Min(1)
                                  @Parameter(description = "Polje popunjeno id") Integer id) {

        return poljePopunjenoHandler.find(id);
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Update polje popunjeno")
    public ResponseEntity<?> update(@Valid
                                    @PathVariable("id")
                                    @Min(1)
                                    @Parameter(description = "Polje popunjeno id") Integer id,
                                    @Valid @RequestBody UpdatePoljePopunjenoRequest updatePoljePopunjenoRequest) {

        return poljePopunjenoHandler.update(id, updatePoljePopunjenoRequest);
    }

    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Delete polje popunjeno")
    public ResponseEntity<String> delete(@Valid
                                       @PathVariable("id")
                                       @Min(1)
                                       @Parameter(description = "Polje popunjeno id") Integer id) {
        return poljePopunjenoHandler.delete(id);
    }
}
