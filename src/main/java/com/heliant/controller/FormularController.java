package com.heliant.controller;

import com.heliant.handler.FormularHandler;
import com.heliant.model.Formular;
import com.heliant.model.dto.CreateFormularRequest;
import com.heliant.model.dto.EntityResponse;
import com.heliant.model.dto.FormularResource;
import com.heliant.model.dto.PaginationResource;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/formular")
public class FormularController {

    private final FormularHandler formularHandler;

    public FormularController(FormularHandler formularHandler) {
        this.formularHandler = formularHandler;
    }

    @PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create formular")
    public EntityResponse<FormularResource> create(@Valid @RequestBody CreateFormularRequest createFormularRequest) {
        return formularHandler.create(createFormularRequest);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get formular")
    public ResponseEntity<?> find(@Valid
                                  @PathVariable("id")
                                  @Min(1)
                                  @Parameter(description = "Formular id") Integer id) {

        return formularHandler.find(id);
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Update formular")
    public ResponseEntity<?> update(@PathVariable("id")
                                    @Min(1)
                                    @Parameter(description = "Formular id") Integer id,
                                    @Valid @Parameter @RequestParam String naziv) {

        return formularHandler.update(id, naziv);
    }

    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Delete formular")
    public ResponseEntity<String> delete(@Valid
                                         @PathVariable("id")
                                         @Min(1)
                                         @Parameter(description = "Formular id") Integer id) {
        return formularHandler.delete(id);
    }

    @GetMapping(path = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get all formulars")
    public EntityResponse<Formular> getAll(@Valid PaginationResource paginationResource,
                                           @RequestParam(defaultValue = "id") String sortBy) {
        return formularHandler.getAll(paginationResource, sortBy);
    }

}
