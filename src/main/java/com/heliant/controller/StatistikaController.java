package com.heliant.controller;

import com.heliant.handler.StatistikaHandler;
import com.heliant.model.dto.CreateStatistikaRequest;
import com.heliant.model.dto.EntityResponse;
import com.heliant.model.dto.StatistikaResource;
import com.heliant.model.dto.UpdateStatistikaRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/statistika")
public class StatistikaController {

    private final StatistikaHandler statistikaHandler;

    public StatistikaController(StatistikaHandler statistikaHandler) {
        this.statistikaHandler = statistikaHandler;
    }

    @PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create statistika")
    public EntityResponse<StatistikaResource> create(@Valid @RequestBody CreateStatistikaRequest createStatistikaRequest) {
        return statistikaHandler.create(createStatistikaRequest);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Find statistika")
    public ResponseEntity<?> find(@Valid
                                  @PathVariable("id")
                                  @Min(1)
                                  @Parameter(description = "Statistika id") Integer id) {

        return statistikaHandler.find(id);
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Update statistika")
    public ResponseEntity<?> update(@Valid
                                    @PathVariable("id")
                                    @Min(1)
                                    @Parameter(description = "Polje popunjeno id") Integer id,
                                    @Valid @RequestBody UpdateStatistikaRequest updateStatistikaRequest) {

        return statistikaHandler.update(id, updateStatistikaRequest);
    }

    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Delete statistika")
    public ResponseEntity<String> delete(@Valid
                                       @PathVariable("id")
                                       @Min(1)
                                       @Parameter(description = "Statistika id") Integer id) {
        return statistikaHandler.delete(id);
    }

}
