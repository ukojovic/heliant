package com.heliant.controller;

import com.heliant.handler.AuthenticationHandler;
import com.heliant.model.dto.AuthenticationRequest;
import com.heliant.model.dto.AuthenticationResponse;
import com.heliant.model.dto.RegisterRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/authentication")
public class AuthenticationController {

    private final AuthenticationHandler authenticationHandler;

    public AuthenticationController(AuthenticationHandler authenticationHandler) {
        this.authenticationHandler = authenticationHandler;
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody RegisterRequest request) {
        return authenticationHandler.register(request);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest request) {
        return ResponseEntity.ok(authenticationHandler.authenticate(request));
    }
}
