package com.heliant.controller;

import com.heliant.handler.FormularPopunjenHandler;
import com.heliant.model.dto.CreateFormularPopunjenRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/formularPopunjen")
public class FormularPopunjenController {

    private final FormularPopunjenHandler formularPopunjenHandler;

    public FormularPopunjenController(FormularPopunjenHandler formularPopunjenHandler) {
        this.formularPopunjenHandler = formularPopunjenHandler;
    }

    @PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create formular popunjen")
    public ResponseEntity<?> create(@Valid @RequestBody CreateFormularPopunjenRequest createFormularPopunjenRequest) {
        return formularPopunjenHandler.create(createFormularPopunjenRequest);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Find formular popunjen")
    public ResponseEntity<?> find(@PathVariable("id")
                                  @Min(1)
                                  @Parameter(description = "Formular popunjen id") Integer id) {

        return formularPopunjenHandler.find(id);
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Update formular popunjen")
    public ResponseEntity<?> update(@Valid
                                    @PathVariable("id")
                                    @Min(1)
                                    @Parameter(description = "Formular popunjen id") Integer id,
                                    @Valid @Parameter @RequestParam Integer formularId) {

        return formularPopunjenHandler.update(id, formularId);
    }

    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Delete formular popunjen")
    public ResponseEntity<String> delete(@Valid
                                       @PathVariable("id")
                                       @Min(1)
                                       @Parameter(description = "Formular popunjen id") Integer id) {
        return formularPopunjenHandler.delete(id);
    }
}
