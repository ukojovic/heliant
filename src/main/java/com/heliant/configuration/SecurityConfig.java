package com.heliant.configuration;

import com.heliant.enumeration.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    private final JwtAuthFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;

    private static final String[] SWAGGER_URL_PATTERNS = new String[]{
            "/v2/api-docs/**",
            "/v3/api-docs/**",
            "/swagger-ui/**",
            "/swagger-ui**",
            "/swagger-resources/**",
            "/webjars/**"
    };

    private static final String[] ADMIN_URL_PATTERNS = new String[]{
            "/api/formular/**",
            "/api/formularPopunjen/**",
            "/api/polje/**",
            "/api/poljePopunjeno/**"
    };

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        http.csrf(AbstractHttpConfigurer::disable)
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
                .authorizeHttpRequests(auth ->
                        auth.requestMatchers("/api/authentication/**").permitAll()
                                .requestMatchers("/api/statistika/**").permitAll()
                                .requestMatchers(ADMIN_URL_PATTERNS).hasAnyAuthority(Role.ADMIN.name())
                                .requestMatchers(SWAGGER_URL_PATTERNS).permitAll()
                                .anyRequest().authenticated()
                )
                .authenticationProvider(authenticationProvider);

        return http.build();

    }
}