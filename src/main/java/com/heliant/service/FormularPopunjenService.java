package com.heliant.service;

import com.heliant.model.Formular;
import com.heliant.model.FormularPopunjen;
import com.heliant.model.dto.CreateFormularPopunjenRequest;
import com.heliant.model.dto.FormularPopunjenResource;
import com.heliant.repository.FormularPopunjenRepository;
import com.heliant.repository.FormularRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Slf4j
public class FormularPopunjenService {

    private final FormularPopunjenRepository formularPopunjenRepository;
    private final FormularRepository formularRepository;

    public FormularPopunjenService(FormularPopunjenRepository formularPopunjenRepository, FormularRepository formularRepository) {
        this.formularPopunjenRepository = formularPopunjenRepository;
        this.formularRepository = formularRepository;
    }

    @Transactional
    public ResponseEntity<?> create(CreateFormularPopunjenRequest createFormularPopunjenRequest) {

        Optional<Formular> optionalFormular = formularRepository.findById(createFormularPopunjenRequest.getIdFormular());

        if (optionalFormular.isPresent()) {

            FormularPopunjen formularPopunjen = FormularPopunjen.builder()
                    .formular(optionalFormular.get())
                    .build();
            FormularPopunjen savedFormularPopunjen = formularPopunjenRepository.save(formularPopunjen);

            FormularPopunjenResource formularPopunjenResource = FormularPopunjenResource.builder()
                    .id(savedFormularPopunjen.getId())
                    .formular(savedFormularPopunjen.getFormular())
                    .vremePoslednjeIzmene(savedFormularPopunjen.getVremePoslednjeIzmene())
                    .vremeKreiranja(savedFormularPopunjen.getVremeKreiranja())
                    .idKorisnikPoslednjiAzurirao(savedFormularPopunjen.getIdKorisnikPoslednjiAzurirao())
                    .idKorisnikKreirao(savedFormularPopunjen.getIdKorisnikKreirao())
                    .build();
            return new ResponseEntity<>(formularPopunjenResource, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Formular nije pronadjen", HttpStatus.NOT_FOUND);
        }

    }

    public Optional<FormularPopunjen> find(Integer id) {
        return formularPopunjenRepository.findById(id);
    }

    @Transactional
    public ResponseEntity<?> update(FormularPopunjen formularPopunjen, Integer formularId) {

        Optional<Formular> optionalFormular = formularRepository.findById(formularId);
        if (optionalFormular.isPresent()) {
            formularPopunjen.setFormular(optionalFormular.get());
            FormularPopunjen updatedFormularPopunjen = formularPopunjenRepository.save(formularPopunjen);

            FormularPopunjenResource formularPopunjenResource = FormularPopunjenResource.builder()
                    .id(updatedFormularPopunjen.getId())
                    .formular(updatedFormularPopunjen.getFormular())
                    .vremeKreiranja(updatedFormularPopunjen.getVremeKreiranja())
                    .vremePoslednjeIzmene(updatedFormularPopunjen.getVremePoslednjeIzmene())
                    .build();

            return new ResponseEntity<>(formularPopunjenResource, HttpStatus.OK);
        }
        return new ResponseEntity<>("Formular kojim zelis da apdejtujes nije pronadjen", HttpStatus.NOT_FOUND);

    }

    public ResponseEntity<String> delete(Integer id) {

        if(formularPopunjenRepository.findById(id).isPresent()){
            formularPopunjenRepository.deleteById(id);
            return new ResponseEntity<>("Obrisano! " + id, HttpStatus.OK);
        }

        return new ResponseEntity<>("Ne postoji formular popunjen s id-ijem " + id, HttpStatus.NOT_FOUND);
    }
}
