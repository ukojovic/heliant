package com.heliant.service;

import com.heliant.model.FormularPopunjen;
import com.heliant.model.Polje;
import com.heliant.model.PoljePopunjeno;
import com.heliant.model.dto.CreatePoljePopunjenoRequest;
import com.heliant.model.dto.UpdatePoljePopunjenoRequest;
import com.heliant.repository.FormularPopunjenRepository;
import com.heliant.repository.PoljePopunjenoRepository;
import com.heliant.repository.PoljeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class PoljePopunjenoService {

    private final PoljePopunjenoRepository poljePopunjenoRepository;
    private final PoljeRepository poljeRepository;
    private final FormularPopunjenRepository formularPopunjenRepository;


    public PoljePopunjenoService(PoljePopunjenoRepository poljePopunjenoRepository, PoljeRepository poljeRepository, FormularPopunjenRepository formularPopunjenRepository) {
        this.poljePopunjenoRepository = poljePopunjenoRepository;
        this.poljeRepository = poljeRepository;
        this.formularPopunjenRepository = formularPopunjenRepository;
    }

    @Transactional
    public ResponseEntity<?> create(CreatePoljePopunjenoRequest createPoljePopunjenoRequest) {

        Optional<Polje> optionalPolje = poljeRepository.findById(createPoljePopunjenoRequest.getIdPolje());
        Optional<FormularPopunjen> optionalFormularPopunjen = formularPopunjenRepository.findById(createPoljePopunjenoRequest.getIdFormularPopunjen());

        if (optionalFormularPopunjen.isEmpty()) {
            return new ResponseEntity<>("Formular popunjen nije pronadjen", HttpStatus.NOT_FOUND);
        }

        if (optionalPolje.isEmpty()) {
            return new ResponseEntity<>("Polje nije pronadjeno", HttpStatus.NOT_FOUND);
        }

        PoljePopunjeno poljePopunjeno = PoljePopunjeno.builder()
                .polje(optionalPolje.get())
                .formularPopunjen(optionalFormularPopunjen.get())
                .vrednostBroj(createPoljePopunjenoRequest.getVrednostBroj())
                .vrednostTekst(createPoljePopunjenoRequest.getVrednostTekst())
                .build();

        PoljePopunjeno updatedPoljePopunjeno = poljePopunjenoRepository.save(poljePopunjeno);
        return new ResponseEntity<>(updatedPoljePopunjeno, HttpStatus.OK);
    }

    public Optional<PoljePopunjeno> find(Integer id) {
        return poljePopunjenoRepository.findById(id);
    }

    public ResponseEntity<String> delete(Integer id) {

        if (poljePopunjenoRepository.findById(id).isPresent()) {
            poljePopunjenoRepository.deleteById(id);
            return new ResponseEntity<>("Obrisano! " + id, HttpStatus.OK);
        }

        return new ResponseEntity<>("Ne postoji polje popunjeno s id-ijem " + id, HttpStatus.NOT_FOUND);
    }

    @Transactional
    public ResponseEntity<?> update(PoljePopunjeno poljePopunjeno, UpdatePoljePopunjenoRequest updatePoljePopunjenoRequest) {

        Optional<Polje> optionalPolje = poljeRepository.findById(updatePoljePopunjenoRequest.getIdPolje());
        if (optionalPolje.isPresent()) {
            poljePopunjeno.setPolje(optionalPolje.get());
        } else {
            return new ResponseEntity<>("Polje nije pronadjeno", HttpStatus.NOT_FOUND);
        }

        Optional<FormularPopunjen> optionalFormularPopunjen = formularPopunjenRepository.findById(updatePoljePopunjenoRequest.getIdFormularPopunjen());
        if (optionalFormularPopunjen.isPresent()) {
            poljePopunjeno.setFormularPopunjen(optionalFormularPopunjen.get());
        } else {
            return new ResponseEntity<>("Formular popunjen nije pronadjen", HttpStatus.NOT_FOUND);
        }

        if (updatePoljePopunjenoRequest.getVrednostBroj() != null)
            poljePopunjeno.setVrednostBroj(updatePoljePopunjenoRequest.getVrednostBroj());
        if (!updatePoljePopunjenoRequest.getVrednostTekst().isEmpty())
            poljePopunjeno.setVrednostTekst(updatePoljePopunjenoRequest.getVrednostTekst());

        PoljePopunjeno updatedPoljePopunjeno = poljePopunjenoRepository.save(poljePopunjeno);
        return new ResponseEntity<>(updatedPoljePopunjeno, HttpStatus.OK);
    }
}
