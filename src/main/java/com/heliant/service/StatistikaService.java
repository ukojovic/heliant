package com.heliant.service;

import com.heliant.model.Statistika;
import com.heliant.model.dto.CreateStatistikaRequest;
import com.heliant.model.dto.UpdateStatistikaRequest;
import com.heliant.repository.FormularPopunjenRepository;
import com.heliant.repository.StatistikaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

@Service
public class StatistikaService {

    private final StatistikaRepository statistikaRepository;
    private final FormularPopunjenRepository formularPopunjenRepository;

    public StatistikaService(StatistikaRepository statistikaRepository, FormularPopunjenRepository formularPopunjenRepository) {
        this.statistikaRepository = statistikaRepository;
        this.formularPopunjenRepository = formularPopunjenRepository;
    }

    @Transactional
    public Statistika create(CreateStatistikaRequest createStatistikaRequest) {

        Statistika statistika = Statistika.builder()
                .datum(createStatistikaRequest.getDatum())
                .brojPopunjenihFormulara(createStatistikaRequest.getBrojPopunjenihFormulara())
                .build();

        return statistikaRepository.save(statistika);
    }

    public Optional<Statistika> find(Integer id) {
        return statistikaRepository.findById(id);
    }

    @Transactional
    public Statistika update(Statistika statistika, UpdateStatistikaRequest updateStatistikaRequest) {

        if (updateStatistikaRequest.getBrojPopunjenihFormulara() != null)
            statistika.setBrojPopunjenihFormulara(updateStatistikaRequest.getBrojPopunjenihFormulara());
        if (updateStatistikaRequest.getDatum() != null)
            statistika.setDatum(updateStatistikaRequest.getDatum());

        return statistikaRepository.save(statistika);
    }

    public ResponseEntity<String> delete(Integer id) {

        if (statistikaRepository.findById(id).isPresent()) {
            statistikaRepository.deleteById(id);
            return new ResponseEntity<>("Obrisano! " + id, HttpStatus.OK);
        }

        return new ResponseEntity<>("Ne postoji statistika s id-ijem " + id, HttpStatus.NOT_FOUND);
    }

    @Transactional
    public void previousDayStatistics() {

        LocalDate previousDay = LocalDate.now().minusDays(1);

        Long brojPopunjenihFormulara = formularPopunjenRepository.previousDayStatistics(
                previousDay.atStartOfDay(), previousDay.atTime(LocalTime.MAX));

        Statistika statistika = Statistika.builder()
                .datum(Date.valueOf(previousDay))
                .brojPopunjenihFormulara(brojPopunjenihFormulara.intValue())
                .build();

        statistikaRepository.save(statistika);
    }
}
