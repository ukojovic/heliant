package com.heliant.service;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class ScheduledTasks {

    private final StatistikaService statistikaService;

    public ScheduledTasks(StatistikaService statistikaService) {
        this.statistikaService = statistikaService;
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void previousDayStatistics() {
        statistikaService.previousDayStatistics();
    }
}
