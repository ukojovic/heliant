package com.heliant.service;

import com.heliant.configuration.JwtService;
import com.heliant.model.Korisnik;
import com.heliant.model.dto.AuthenticationRequest;
import com.heliant.model.dto.AuthenticationResponse;
import com.heliant.model.dto.RegisterRequest;
import com.heliant.repository.KorisnikRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class AuthenticationService {

    private final KorisnikRepository korisnikRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public AuthenticationService(KorisnikRepository korisnikRepository, BCryptPasswordEncoder bCryptPasswordEncoder, JwtService jwtService, AuthenticationManager authenticationManager) {
        this.korisnikRepository = korisnikRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.jwtService = jwtService;
        this.authenticationManager = authenticationManager;
    }

    @Transactional
    public ResponseEntity<?> register(RegisterRequest request) {

        if (korisnikRepository.findByKorisnickoIme(request.getKorisnickoIme()).isPresent()) {
            return new ResponseEntity<>("Vec postoji korisnik s ovim korisnickim imenom", HttpStatus.BAD_REQUEST);
        }

        Korisnik korisnik = Korisnik.builder()
                .korisnickoIme(request.getKorisnickoIme())
                .lozinka(bCryptPasswordEncoder.encode(request.getLozinka()))
                .vremeKreiranja(LocalDateTime.now())
                .vremePoslednjeIzmene(LocalDateTime.now())
                .role(request.getRole())
                .build();

        String jwtToken = jwtService.generateToken(korisnik);
        korisnikRepository.save(korisnik);
        return new ResponseEntity<>(AuthenticationResponse.builder().token(jwtToken).build(), HttpStatus.OK);
    }

    @Transactional
    public AuthenticationResponse authenticate(AuthenticationRequest request) {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getKorisnickoIme(),
                        request.getLozinka()
                )
        );

        Korisnik korisnik = korisnikRepository.findByKorisnickoIme(request.getKorisnickoIme())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        String jwtToken = jwtService.generateToken(korisnik);
        korisnikRepository.save(korisnik);
        return AuthenticationResponse.builder().token(jwtToken).build();
    }

    public static Integer getCurrentUserId() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ((Korisnik) principal).getId();
    }
}
