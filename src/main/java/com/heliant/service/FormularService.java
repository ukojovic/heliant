package com.heliant.service;

import com.heliant.model.Formular;
import com.heliant.model.dto.CreateFormularRequest;
import com.heliant.model.dto.EntityResponse;
import com.heliant.model.dto.PaginationResource;
import com.heliant.repository.FormularRepository;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class FormularService {

    private final FormularRepository formularRepository;

    public FormularService(FormularRepository formularRepository) {
        this.formularRepository = formularRepository;
    }

    @Transactional
    public Formular create(CreateFormularRequest createFormularRequest) {

        Formular formular = Formular.builder()
                .naziv(createFormularRequest.getNaziv())
                .build();

        return formularRepository.save(formular);
    }

    public Optional<Formular> find(Integer id) {
        return formularRepository.findById(id);
    }

    @Transactional
    public Formular update(Formular formular, String naziv) {

        formular.setNaziv(naziv);
        return formularRepository.save(formular);
    }

    @SneakyThrows
    public ResponseEntity<String> delete(Integer id) {

        if (formularRepository.findById(id).isPresent()) {
            formularRepository.deleteById(id);
            return new ResponseEntity<>("Obrisano! " + id, HttpStatus.OK);
        }

        return new ResponseEntity<>("Ne postoji formular s id-ijem " + id, HttpStatus.NOT_FOUND);

    }

    public EntityResponse<Formular> getAll(PaginationResource paginationResource, String sortBy) {

        int page = 0;
        int offset = paginationResource.getOffset();
        int limit = paginationResource.getLimit();
        if (offset >= limit) {
            page = offset / limit;
        }

        Pageable pageable = PageRequest.of(page, limit, Sort.by(sortBy));
        Page<Formular> formulars = formularRepository.findAll(pageable);

        return EntityResponse.<Formular>builder()
                .result(formulars.getContent())
                .totalCount(formulars.getTotalElements())
                .build();
    }
}
