package com.heliant.service;

import com.heliant.model.Formular;
import com.heliant.model.Polje;
import com.heliant.model.dto.CreatePoljeRequest;
import com.heliant.model.dto.PoljeResource;
import com.heliant.model.dto.UpdatePoljeRequest;
import com.heliant.repository.PoljeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Slf4j
public class PoljeService {

    private final PoljeRepository poljeRepository;
    private final FormularService formularService;

    public PoljeService(PoljeRepository poljeRepository, FormularService formularService) {
        this.poljeRepository = poljeRepository;
        this.formularService = formularService;
    }

    @Transactional
    public ResponseEntity<?> create(CreatePoljeRequest createPoljeRequest) {

        Optional<Formular> formular = formularService.find(createPoljeRequest.getIdFormular());

        if (formular.isPresent()) {
            Polje polje = Polje.builder()
                    .tip(createPoljeRequest.getTip())
                    .naziv(createPoljeRequest.getNaziv())
                    .prikazniRedosled(createPoljeRequest.getPrikazniRedosled())
                    .formular(formular.get())
                    .build();
            Polje savedPolje = poljeRepository.save(polje);
            PoljeResource poljeResource = PoljeResource.builder()
                    .id(savedPolje.getId())
                    .naziv(savedPolje.getNaziv())
                    .tip(savedPolje.getTip())
                    .vremePoslednjeIzmene(savedPolje.getVremePoslednjeIzmene())
                    .vremeKreiranja(savedPolje.getVremeKreiranja())
                    .prikazniRedosled(savedPolje.getPrikazniRedosled())
                    .formular(savedPolje.getFormular())
                    .idKorisnikKreirao(savedPolje.getIdKorisnikKreirao())
                    .idKorisnikPoslednjiAzurirao(savedPolje.getIdKorisnikPoslednjiAzurirao())
                    .build();

            return new ResponseEntity<>(poljeResource, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Formular nije pronadjen", HttpStatus.NOT_FOUND);
        }
    }

    public Optional<Polje> find(Integer id) {
        return poljeRepository.findById(id);
    }

    @Transactional
    public ResponseEntity<?> update(Integer id, UpdatePoljeRequest updatePoljeRequest) {

        Optional<Polje> optionalPolje = poljeRepository.findById(id);

        if (optionalPolje.isPresent()) {
            Polje polje = optionalPolje.get();

            if (polje.getFormular().getId() != updatePoljeRequest.getFormularId()) {
                Optional<Formular> optionalFormular = formularService.find(updatePoljeRequest.getFormularId());
                if (optionalFormular.isPresent()) {
                    polje.setFormular(optionalFormular.get());
                } else {
                    return new ResponseEntity<>("Formular nije pronadjen", HttpStatus.NOT_FOUND);
                }
            }

            if (updatePoljeRequest.getTip() != null)
                polje.setTip(updatePoljeRequest.getTip());
            if (updatePoljeRequest.getPrikazniRedosled() != null)
                polje.setPrikazniRedosled(updatePoljeRequest.getPrikazniRedosled());
            if (updatePoljeRequest.getNaziv() != null)
                polje.setNaziv(updatePoljeRequest.getNaziv());

            Polje updatedPolje = poljeRepository.save(polje);
            return new ResponseEntity<>(updatedPolje, HttpStatus.OK);

        }
        return new ResponseEntity<>("Polje nije pronadjeno", HttpStatus.NOT_FOUND);

    }

    public ResponseEntity<String> delete(Integer id) {

        if (poljeRepository.findById(id).isPresent()) {
            poljeRepository.deleteById(id);
            return new ResponseEntity<>("Obrisano! " + id, HttpStatus.OK);
        }

        return new ResponseEntity<>("Ne postoji polje s id-ijem " + id, HttpStatus.NOT_FOUND);

    }
}
