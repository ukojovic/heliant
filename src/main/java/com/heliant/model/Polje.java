package com.heliant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.heliant.enumeration.Tip;
import com.heliant.service.AuthenticationService;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "polje")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Polje {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_formular", nullable = false)
    private Formular formular;

    @Column(name = "naziv")
    private String naziv;

    @Column(name = "prikazni_redosled")
    private Integer prikazniRedosled;

    @Column(name = "tip")
    @Enumerated(EnumType.STRING)
    private Tip tip;

    @Column(name = "vreme_kreiranja")
    private LocalDateTime vremeKreiranja;

    @Column(name = "vreme_poslednje_izmene")
    private LocalDateTime vremePoslednjeIzmene;

    @Column(name = "id_korisnik_kreirao", nullable = false)
    private Integer idKorisnikKreirao;

    @Column(name = "id_korisnik_poslednji_azurirao", nullable = false)
    private Integer idKorisnikPoslednjiAzurirao;

    @OneToMany(mappedBy = "polje", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    private List<PoljePopunjeno> poljePopunjenoList;

    @PrePersist
    public void prePersist() {
        Integer createdByUser = AuthenticationService.getCurrentUserId();
        this.idKorisnikKreirao = createdByUser;
        this.idKorisnikPoslednjiAzurirao = createdByUser;
        this.vremeKreiranja = LocalDateTime.now();
        this.vremePoslednjeIzmene = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        this.vremePoslednjeIzmene = LocalDateTime.now();
        this.idKorisnikPoslednjiAzurirao = AuthenticationService.getCurrentUserId();
    }
}
