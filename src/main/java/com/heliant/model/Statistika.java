package com.heliant.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@Table(name = "statistika")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Statistika {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "datum")
    private Date datum;

    @Column(name = "broj_popunjenih_formulara")
    private Integer brojPopunjenihFormulara;

}
