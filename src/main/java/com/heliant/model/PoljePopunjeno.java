package com.heliant.model;

import com.heliant.service.AuthenticationService;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Table(name = "polje_popunjeno")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PoljePopunjeno {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_formular_popunjen", nullable = false)
    private FormularPopunjen formularPopunjen;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_polje", nullable = false)
    private Polje polje;

    @Column(name = "vrednost_tekst")
    private String vrednostTekst;

    @Column(name = "vrednost_broj")
    private Integer vrednostBroj;

    @Column(name = "vreme_kreiranja")
    private LocalDateTime vremeKreiranja;

    @Column(name = "vreme_poslednje_izmene")
    private LocalDateTime vremePoslednjeIzmene;

    @Column(name = "id_korisnik_kreirao", nullable = false)
    private Integer idKorisnikKreirao;

    @Column(name = "id_korisnik_poslednji_azurirao", nullable = false)
    private Integer idKorisnikPoslednjiAzurirao;

    @PrePersist
    public void prePersist() {
        Integer createdByUser = AuthenticationService.getCurrentUserId();
        this.idKorisnikKreirao = createdByUser;
        this.idKorisnikPoslednjiAzurirao = createdByUser;
        this.vremeKreiranja = LocalDateTime.now();
        this.vremePoslednjeIzmene = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        this.vremePoslednjeIzmene = LocalDateTime.now();
        this.idKorisnikPoslednjiAzurirao = AuthenticationService.getCurrentUserId();
    }
}
