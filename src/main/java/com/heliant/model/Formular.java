package com.heliant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.heliant.service.AuthenticationService;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "formular")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
public class Formular {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column
    private String naziv;

    @Column(name = "vreme_kreiranja")
    private LocalDateTime vremeKreiranja;

    @Column(name = "vreme_poslednje_izmene")
    private LocalDateTime vremePoslednjeIzmene;

    @Column(name = "id_korisnik_kreirao", nullable = false)
    private Integer idKorisnikKreirao;

    @Column(name = "id_korisnik_poslednji_azurirao", nullable = false)
    private Integer idKorisnikPoslednjiAzurirao;

    @OneToMany(mappedBy = "formular", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    private List<FormularPopunjen> formularPopunjenList;

    @PrePersist
    public void prePersist() {
        Integer createdByUser = AuthenticationService.getCurrentUserId();
        this.idKorisnikKreirao = createdByUser;
        this.idKorisnikPoslednjiAzurirao = createdByUser;
        this.vremeKreiranja = LocalDateTime.now();
        this.vremePoslednjeIzmene = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        this.vremePoslednjeIzmene = LocalDateTime.now();
        this.idKorisnikPoslednjiAzurirao = AuthenticationService.getCurrentUserId();
    }
}
