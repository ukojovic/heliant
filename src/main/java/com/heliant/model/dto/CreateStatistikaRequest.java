package com.heliant.model.dto;

import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class CreateStatistikaRequest {

    @NotNull(message = "Datum mora biti unet")
    private Date datum;

    @NotNull(message = "Broj popunjenih formulara mora biti unet")
    private Integer brojPopunjenihFormulara;
}
