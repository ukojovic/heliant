package com.heliant.model.dto;

import com.heliant.enumeration.Tip;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class UpdatePoljeRequest {

    private int formularId;

    private String naziv;

    private Integer prikazniRedosled;

    private Tip tip;
}
