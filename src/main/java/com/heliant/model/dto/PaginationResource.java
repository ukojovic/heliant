package com.heliant.model.dto;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class PaginationResource {

    @Min(0)
    @NotNull(message = "Parameter offset if missing in the request")
    @Parameter(
            description = "Offset number from where to start fetching records. Starting at 0",
            schema = @Schema(
                    description = "Offset number from where to start fetching records. Starting at 0",
                    example = "0",
                    type = "integer"
            )
    )
    private Integer offset;

    @Positive(message = "Parameter limit must be larger than 0")
    @NotNull(message = "Parameter limit is missing in the request")
    @Min(1)
    @Parameter(
            description = "Limit to number of records retrieved",
            schema = @Schema(
                    description = "Limit to number of records retrieved",
                    example = "50",
                    type = "integer"
            )
    )
    private Integer limit;
}
