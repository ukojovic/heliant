package com.heliant.model.dto;

import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class FormularResource {

    private Integer id;

    private String naziv;

    private LocalDateTime vremeKreiranja;

    private LocalDateTime vremePoslednjeIzmene;

    private Integer idKorisnikKreirao;

    private Integer idKorisnikPoslednjiAzurirao;

}
