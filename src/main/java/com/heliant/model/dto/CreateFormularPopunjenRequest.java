package com.heliant.model.dto;

import jakarta.validation.constraints.NotNull;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class CreateFormularPopunjenRequest {

    @NotNull(message = "Id formulara mora biti unet.")
    private int idFormular;

}
