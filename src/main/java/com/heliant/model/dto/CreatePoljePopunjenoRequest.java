package com.heliant.model.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class CreatePoljePopunjenoRequest {

    @NotNull(message = "Id formular popunjen mora biti unet.")
    private Integer idFormularPopunjen;

    @NotNull(message = "Id polja mora biti unet.")
    private Integer idPolje;

    @NotBlank(message = "Vrednost teksta mora biti uneta")
    private String vrednostTekst;

    @Min(value = 1, message = "Vrednost broja mora biti uneta i veca ili jednaka od 1.")
    private Integer vrednostBroj;
}
