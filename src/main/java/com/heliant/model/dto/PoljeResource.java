package com.heliant.model.dto;

import com.heliant.enumeration.Tip;
import com.heliant.model.Formular;
import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class PoljeResource {

    private Integer id;

    private Formular formular;

    private String naziv;

    private Integer prikazniRedosled;

    private Tip tip;

    private LocalDateTime vremeKreiranja;

    private LocalDateTime vremePoslednjeIzmene;

    private Integer idKorisnikKreirao;

    private Integer idKorisnikPoslednjiAzurirao;

}
