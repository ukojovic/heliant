package com.heliant.model.dto;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class StatistikaResource {

    private Integer id;

    private Date datum;

    private Integer brojPopunjenihFormulara;
}
