package com.heliant.model.dto;

import jakarta.validation.constraints.Min;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class UpdatePoljePopunjenoRequest {

    @Min(value = 1, message = "Id popunjenog formulara mora biti veci od nule")
    private Integer idFormularPopunjen;

    @Min(1)
    private Integer idPolje;

    private String vrednostTekst;

    private Integer vrednostBroj;

}
