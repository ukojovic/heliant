package com.heliant.model.dto;

import jakarta.validation.constraints.Min;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class UpdateStatistikaRequest {

    private Date datum;

    @Min(0)
    private Integer brojPopunjenihFormulara;
}
