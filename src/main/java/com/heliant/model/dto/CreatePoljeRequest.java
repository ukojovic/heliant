package com.heliant.model.dto;

import com.heliant.enumeration.Tip;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class CreatePoljeRequest {

    @NotNull(message = "Id formulara mora biti unet.")
    private int idFormular;

    @NotBlank(message = "Naziv polja ne sme biti prazan.")
    private String naziv;

    @Min(1)
    private int prikazniRedosled;

    @NotNull
    private Tip tip;
}
