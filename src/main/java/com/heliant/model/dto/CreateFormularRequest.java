package com.heliant.model.dto;

import jakarta.validation.constraints.NotNull;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class CreateFormularRequest {

    @NotNull(message = "Naziv formulara mora biti unet.")
    private String naziv;

}
