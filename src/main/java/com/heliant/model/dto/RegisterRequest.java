package com.heliant.model.dto;

import com.heliant.enumeration.Role;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class RegisterRequest {

    private String fullName;
    private String lozinka;
    private String korisnickoIme;
    private Role role;
}
