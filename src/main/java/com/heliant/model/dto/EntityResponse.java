package com.heliant.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class EntityResponse<T>
{
    private List<T> result;

    @Schema(description = "total count for results", type = "number")
    private Long totalCount;
}