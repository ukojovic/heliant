package com.heliant.model.dto;

import com.heliant.model.Formular;
import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class FormularPopunjenResource {

    private Integer id;

    private Formular formular;

    private LocalDateTime vremeKreiranja;

    private LocalDateTime vremePoslednjeIzmene;

    private Integer idKorisnikKreirao;

    private Integer idKorisnikPoslednjiAzurirao;


}
