package com.heliant.model.dto;

import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class PoljePopunjenoResource {

    private Integer id;

    private FormularPopunjenResource formularPopunjen;

    private PoljeResource polje;

    private String vrednostTekst;

    private Integer vrednostBroj;

    private LocalDateTime vremeKreiranja;

    private LocalDateTime vremePoslednjeIzmene;

    private Integer idKorisnikKreirao;

    private Integer idKorisnikPoslednjiAzurirao;

}

